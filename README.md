# agent-smith
Agent Smith is a [Matrix](https://matrix.org) reaction bot that runs solely in the browser.
It listens for user selected trigger words and can react either with an emoji or a text message.  
See https://agent-smith.app  

Currently only unencrypted rooms are supported.

## Project setup
If you want to hack it start with:
```bash
npm install
```

### Compiles and hot-reloads for development
```bash
npm run serve
```

### Compiles and minifies for production
```bash
npm run build
```

### Compiles and minifies for more recent browsers
```bash
npm run build-modern
```

### Lints and fixes files
```bash
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
