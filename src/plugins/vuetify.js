import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: true,
    themes: {
      light: {
        primary: '#ee44aa',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
      },
      dark: {
        primary: colors.green.darken3,
        secondary: colors.deepPurple.darken2,
        accent: colors.blueGrey.darken1,
        error: colors.red.darken3,
        warning: colors.amber.darken1,
        info: colors.teal.lighten1,
        success: colors.lightGreen.accent4,
      },
    },
  },
});
